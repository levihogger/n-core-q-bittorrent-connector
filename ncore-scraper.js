if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const axios = require("axios")
const { ConcurrencyManager } = require("axios-concurrency");


const MAX_CONCURRENT_REQUESTS = 3;
const categoryMap = {
    "xvid_hun":"Film SD\/HU",
    "xvid":"Film SD\/EN",
    "dvd_hun":"Film DVDR\/HU",
    "dvd":"Film DVDR\/EN",
    "dvd9_hun":"Film DVD9\/HU",
    "dvd9":"Film DVD9\/EN",
    "hd_hun":"Film HD\/HU",
    "hd":"Film HD\/EN",
    
    "xvidser_hun":"Sorozat SD\/HU",
    "xvidser":"Sorozat SD\/EN",
    "dvdser_hun":"Sorozat DVDR\/HU",
    "dvdser":"Sorozat DVDR\/EN",
    "hdser_hun":"Sorozat HD\/HU",
    "hdser":"Sorozat HD\/EN",
    
    "mp3_hun":"MP3\/HU",
    "mp3":"MP3\/EN",
    "lossless_hun":"Lossless\/HU",
    "lossless":"Lossless\/EN",
    "clip":"Klip",

    "game_iso":"J\u00e1t\u00e9k PC\/ISO",
    "game_rip":"J\u00e1t\u00e9k PC\/RIP",
    "console":"Konzol",

    "ebook_hun":"eBook\/HU",
    "ebook":"eBook\/EN",

    "iso":"APP\/ISO",
    "misc":"APP\/RIP",
    "mobil":"APP\/Mobil",
    
    "xxx_xvid":"XXX SD",
    "xxx_dvd":"XXX DVDR\/DVD9",
    "xxx_imageset":"XXX Imageset",
    "xxx_hd":"XXX HD"
};
const options = {
    nCoreUser: process.env.NCORE_USER,
    nCorePass: process.env.NCORE_PASSWORD,
}

const categoryToType = (category) => {
    const movieCategories = ["xvid_hun", "xvid", "dvd_hun", "dvd", "dvd9_hun", "dvd9", "hd_hun", "hd"];
    const seriesCategories = ["xvidser_hun", "xvidser", "dvdser_hun", "dvdser", "hdser_hun", "hdser"];
    const musicCategories = ["mp3_hun", "mp3", "lossless_hun", "lossless", "clip"];
    const gameCategories = ["game_iso", "game_rip", "console"];
    const ebookCategories = ["ebook_hun", "ebook"];
    const appCategories = ["iso", "misc", "mobil"];
    const xxxCategories = ["xxx_xvid", "xxx_dvd", "xxx_imageset", "xxx_hd"];

    if(movieCategories.includes(category)){
        return "Movies";
    }
    else if(seriesCategories.includes(category)){
        return "Series";
    }
    else if(musicCategories.includes(category)){
        return "Music";
    }
    else if(gameCategories.includes(category)){
        return "Games";
    }
    else if(ebookCategories.includes(category)){
        return "Ebooks";
    }
    else if(appCategories.includes(category)){
        return "Apps";
    }
    else if(xxxCategories.includes(category)){
        return "XXX";
    }
    else{
        return "Other";
    }
}

const instance = axios.create({
  baseURL: 'https://ncore.pro',
  headers: {
      Cookie: `nick=${options.nCoreUser}; pass=${options.nCorePass};`,
    },
});

const manager = ConcurrencyManager(instance, MAX_CONCURRENT_REQUESTS);

async function search(query){
    let data = (await instance.get(`/torrents.php?oldal=1&tipus=all&mire=${encodeURIComponent(query)}&miben=name`)).data
    let torrentData = parseSearchPage(data)

    if(data.includes('<div class="lista_mini_error">Nincs találat!</div>'))
    {
        return []
    }
    pages = data.split('pager_top')[1].split('</div>')[0].split(' | ');
    if(pages.length > 1){
        pages.shift();
        let last = pages.filter(el => el.includes("Utolsó"))[0]
        let lastPage;
        if(last != undefined){
            lastPage = last.split("?oldal=")[1].split("&")[0];
        }
        else{
            lastPage = pages.length;
        }
        pages = Array.from({length:lastPage-1},(v,k)=>k+2)
        pages = pages.map(el => `/torrents.php?oldal=${el}&tipus=all&mire=${encodeURIComponent(query)}&miben=name`)
        let results = await Promise.all(pages.map(el => instance.get(el)));
        results = results.map(el => parseSearchPage(el.data));
        results = results.flat();
        torrentData = torrentData.concat(results);
    }
    return torrentData.sort((a,b) => b.seeders - a.seeders).filter(el => el.seeders!=undefined)
}

async function searchIMDBOnly(query){
    return new Promise((resolve, reject) => {
        search(query).then(async data => {
            let j = data.filter(el => el.IMDBid != undefined && !el.type.typeName.includes("xxx"))
            j = groupByIMDB(j);
            return resolve(Object.values(j))
        })
    })
}

function groupByIMDB(data){
    let j = {};
    data.forEach(el => {
        if(j[el.IMDBid] == undefined){
            j[el.IMDBid] = {
                IMDBid: el.IMDBid,
                torrents: [el]
            }
        }
        else{
            j[el.IMDBid].torrents.push(el);
        }
    })

    return j;
}

function torrentDataMapper(data){
    const type = {
        typeName: data.split('tipus=')[1].split('"')[0],
        alt: data.split('tipus=')[1].split('alt="')[1]?.split('"')[0],
        description:data.split('tipus=')[1].split('title="')[1].split('"')[0],
        type2: categoryMap[data.split('tipus=')[1].split('"')[0]],
        type3: categoryToType(data.split('tipus=')[1].split('"')[0])
    }
    const size = data.split('<div class="box_meret2">')[1]?.split('</div>')[0];
    const id = data.split('torrents.php?action=details&id=')[1]?.split('"')[0]
    const seeders = data.split('peers">')[1]?.split('</a>')[0];
    const leechers = data.split('peers">')[2]?.split('</a>')[0];

    const name = data.split('<div class="torrent_txt"')[1]?.split('title="')[1].split('"')[0] || data.split('title="')[2]?.split('"')[0]
    const IMDBid = data.split('<div class="torrent_txt"')[1]?.split('https://imdb.com/title/')[1]?.split('/')[0]
    return {type, size, seeders, leechers, id, name, IMDBid}
}

function parseSearchPage(data){
    let torrentData = data.split('"box_torrent"');
    torrentData = torrentData.map(torrentDataMapper)
    return torrentData;
}

module.exports = {
    search,
    searchIMDBOnly,
}