const axios = require('axios');

axios.defaults.headers.common['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36';

const getByImdbId = async (imdbId) => {
    if(!imdbId.startsWith('tt')){
        return "tt"+imdbId;
    }

    const { data } = await axios.get(`https://www.imdb.com/title/${imdbId}/`);

    const title = data.split('<title>')[1].split('</title>')[0].split(' - IMDb')[0].split(' (')[0];
    const year = data.split('<title>')[1].split('</title>')[0].split(' - IMDb')[0].split(' (')[1].split(')')[0].split(" ").pop();
    const poster = data.split('loading="eager"')[1].split('src="')[1].split('"')[0].split("V1")[0]+"V1_.jpg";
    const description = data.split('data-testid="plot-xl"')[1].split('</span>')[0].split('>')[1].split("<")[0];
    const rating = data.split('ratingValue":').pop().split('"')[0].split("}")[0];

    return {
        id: imdbId,
        title,
        year,
        poster,
        description,
        rating
    }
}

module.exports = getByImdbId;